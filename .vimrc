"------------------------------------------------------------
" http://vim.wikia.com/wiki/Mapping_keys_in_Vim_-_Tutorial_(Part_1)
" Set 'nocompatible' to ward off unexpected things that your distro might have made, as well as sanely reset options when re-sourcing .vimrc
" Plugin 'gmarik/vundle'
" call vundle#rc()

" Vundle *******************************************************************
" first install from git clone https://github.com/VundleVim/Vundle.vim.git
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
Plugin 'scrooloose/nerdtree.git'
Plugin 'Buffergator'
Plugin 'kien/ctrlp.vim'
Plugin 'archernar/polymode.vim'
Plugin 'easymotion/vim-easymotion'
Plugin 'tpope/vim-fugitive'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'mbbill/undotree'
Plugin 'NLKNguyen/papercolor-theme'
call vundle#end()
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
" filetype plugin on
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
" see :h vundle for more details or wiki for FAQ
" Vundle *******************************************************************Put your non-Plugin stuff after this line




let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"

" Leader - ( Spacebar )
let mapleader = " "
nnoremap <Leader>' diwi""<ESC>hp<ESC>
nnoremap <Leader>nt :NERDTreeToggle<cr>
nnoremap <Leader>p  :PluginUpdate<cr>

nnoremap <leader>ev :split $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

"
" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
" filetype indent plugin on
" filetype plugin on 
 
 
"------------------------------------------------------------
" Must have options {{{1
"
" These are highly recommended options.
 
" Vim with default settings does not allow easy switching between multiple files
" in the same editor window. Users can use multiple split windows or multiple
" tab pages to edit multiple files, but it is still best to enable an option to
" allow easier switching between files.
"
" One such option is the 'hidden' option, which allows you to re-use the same
" window and switch from an unsaved buffer without saving it first. Also allows
" you to keep an undo history for multiple files when re-using the same window
" in this way. Note that using persistent undo also lets you undo in multiple
" files even in the same window, but is less efficient and is actually designed
" for keeping undo history after closing Vim entirely. Vim will complain if you
" try to quit without saving, and swap files will keep you safe if your computer
" crashes.
" set hidden
 
" Note that not everyone likes working this way (with the hidden option).
" Alternatives include using tabs or split windows instead of re-using the same
" window as mentioned above, and/or either of the following options:
" set confirm
" set autowriteall
 
syntax off                        " Enable syntax highlighting
set ruler                         " Display the cursor position on the last line of the screen or in the status line of a window
set number                        " Display line numbers on the left
set wildmenu                      " Better command-line completion
set showcmd                       " Show partial commands in the last line of the screen
set ignorecase                    " Use case insensitive search, except when using capital letters
set smartcase
set hlsearch incsearch            " Highlight searches (use <C-L> to temporarily turn off highlighting; see the mapping of <C-L> below)
set backspace=indent,eol,start    " Allow backspacing over autoindent, line breaks and start of insert action
set nostartofline                 " Stop certain movements from always going to the first character of a line.
set laststatus=2                  " Always display the status line, even if only one window is displayed
set autoindent                    " When opening a new line and no filetype-specific indenting is enabled, keep same indent as line currently on.
set confirm                       " Instead of failing a command because of unsaved changes, raise a dialogue asking to save changed files.
set visualbell                    " Use visual bell instead of beeping when doing something wrong
set t_vb=
                                  " reset terminal code for visual bell. 
                                  " If visualbell is set, and this line is also included, vim will neither flash nor beep.
                                  " If visualbell is unset, this does nothing.
set cmdheight=2                   " Set the command window height to 2 lines, to avoid many cases of having to  press <Enter> to continue
set pastetoggle=<F11>             " Use <F11> to toggle between 'paste' and 'nopaste'
set shiftwidth=4                  " Indent settings for using 4 spaces instead of tabs.  Do not change 'tabstop' from its default value of 8 
set softtabstop=4                 " with this setup.
set expandtab

" set mouse=a
" Enable use of the mouse for all modes
 
set notimeout ttimeout ttimeoutlen=200         " Quickly time out on keycodes, but never time out on mappings
 
 
 
 
"------------------------------------------------------------
" Indentation options {{{1
"
" Indentation settings according to personal preference.
 
 
" Indentation settings for using hard tabs for indent. Display tabs as
" four characters wide.
"set shiftwidth=4
"set tabstop=4
 
 
"------------------------------------------------------------
" Mappings {{{1
"
" Useful mappings
 
" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,
" which is the default
map Y y$
 
" Map <C-L> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-L> :nohl<CR><C-L>

"------------------------------------------------------------
" http://vim.wikia.com/wiki/Easier_buffer_switching
" http://eseth.org/2007/vim-buffers.html
" http://vim.wikia.com/wiki/Switch_between_Vim_window_splits_easily
" http://superuser.com/questions/486532/how-to-open-files-in-vertically-horizontal-split-windows-in-vim-from-the-command
" http://stackoverflow.com/questions/3776117/what-is-the-difference-between-the-remap-noremap-nnoremap-and-vnoremap-mapping
" :map and :noremap are recursive and non-recursive versions of the various mapping commands.


nnoremap <F5> :buffers<CR>:buffer<Space>

nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>
nnoremap <F3> :set nonumber!<CR>
:map <F2> <C-W>w                                            "-- Cycle Window
:map <F3> :bnext<CR>                                        "-- Cycle Buffer
:map <F4> <C-w>s                                            "-- Split Window
:map <F5> <C-w>v                                            "-- Split Window

"-- Window Setup
:map <F9> <C-w>s<CR><C-w>v<CR>
:map <F8> :BuffergatorToggle<cr>


:nmap <F6> 'a

"-- Build
":map <F12> <ESC>:wa<CR>:!fig -a dist<CR><CR>
:map <F12> <ESC>:wa<CR>:!.figbuild<CR><CR>


set  rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256


"
" zoom a vim pane, <C-w>= to re-balance
" resize panes
" nnoremap <silent> <Right> :vertical resize +5<cr>
" nnoremap <silent> <Left> :vertical resize -5<cr>
nnoremap <leader>-        :wincmd _<cr>:wincmd \|<cr>
nnoremap <leader>=        :wincmd =<cr>
nnoremap <leader>l        :resize -5<cr>
nnoremap <leader>m        :resize +5<cr>
nnoremap <leader>n        :call PolyMode()<cr>
nnoremap <Home>           :call PolyMode()<cr>
nnoremap <End>            :call Poly2Mode()<cr>


"hi Normal ctermbg=black
hi Visual cterm=NONE ctermfg=black ctermbg=white
hi Search cterm=NONE ctermfg=white ctermbg=red
highlight ColorColumn  ctermbg=red
set colorcolumn=0

nnoremap <silent> n n:call HLNext(0.4)<cr>
nnoremap <silent> N N:call HLNext(0.4)<cr>
nnoremap <silent> ;; :s/^/"/<cr>:s/$/"/<cr>:s/https.*v=//<cr>:s/&list.*"//<cr>:s/$/"/<cr>
nnoremap <silent> ;p :s/^.*http/http/<cr>:s/http.*watch[?]v=//<cr>:s/[&].*$//<cr>:s/$/",/<cr>:s/^/"/<cr>
" :s/$/"/<cr>:s/https.*v=//<cr>:s/&list.*"//<cr>:s/$/"/<cr>
vmap  <expr>  h        DVB_Drag('left')
vmap  <expr>  l        DVB_Drag('right')
vmap  <expr>  j        DVB_Drag('down')
vmap  <expr>  k        DVB_Drag('up')

"function Test()
"    let fn=input("Filename: ", "")
"    split
"    wincmd w
"    exec "e ".fn 
"    return 0 
"endfunction
"nnoremap <leader>' :call Test()<cr>
"autocmd WinEnter * :set cursorline
"autocmd WinEnter * :redraw
"autocmd WinEnter * :sleep 1
"autocmd WinEnter * :set nocursorline

colorscheme molokai

